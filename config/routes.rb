Rails.application.routes.draw do

  class CustomDomainConstraint
    # Implement the .matches? method and pass in the request object
    def self.matches? request
      matching_site?(request)
    end

    def self.matching_site? request
      # handle the case of the user's domain being either www. or a root domain with one query
      if request.subdomain == 'www'
        req = request.host[4..-1]
      else
        req = request.host
      end
      @web_url = "https://api.winkl.in"
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '}
      blogs = HTTParty.get(@web_url+"/customblogs?customdomain="+req.to_s, headers: headers)
      if blogs.length == 0
       blogs = HTTParty.get(@web_url+"/subdomainblogs/"+request.subdomain, headers: headers)
      end
      blogs.length>0
      # true
    end

  end

  match '/', :to => 'blog#show', :constraints => CustomDomainConstraint, via: :all
  get '/', to: 'home#show', constraints: { subdomain: 'www' }
  # get '/', to: 'blog#show', constraints: { subdomain: /.+/ }
  root 'home#show'
  get '/content/:slug', to: 'content#getslugcontent', constraints: { subdomain: '' }
  get '/content/:slug', to: 'content#getslugcontent', constraints: { subdomain: 'www' }
  get '/content/:slug', to: 'blog#getslugcontent', :constraints => CustomDomainConstraint, via: :all
  get '/tabs/:category', to: 'blog#category', :constraints => CustomDomainConstraint, via: :all
  get '/create', to: 'create#create', :constraints => CustomDomainConstraint, via: :all
  get '/contact', to: 'about#contact', constraints: {subdomain: 'www'}
  get '/contact', to: 'blog#contact', :constraints => CustomDomainConstraint, via: :all
  get '/about', to: 'about#about', constraints: {subdomain: 'www'}
  get '/about', to: 'blog#about', :constraints => CustomDomainConstraint, via: :all
  get '/about/edit', to: 'blog#about_edit', :constraints => CustomDomainConstraint, via: :all
  get '/edit', to: 'blog#edit', :constraints => CustomDomainConstraint, via: :all
  get '/add_insta', to: 'bloghome#add_insta', :constraints => CustomDomainConstraint, via: :all
  get '/addinstatowinkl', to: 'profile#addinsta'
  post '/follow_blog', to: 'blog#follow'

  get '/product_hunt', to: 'product_hunt#show'
  get '/tutorial', to: 'tutorial#show'
  get '/diys', to: 'hack#show'
  get '/home', to: 'home#show'
  get '/blogger', to: 'bloghome#landing'
  get '/create_blog', to: 'blog#index'
  post '/create_blog', to: 'create#blog'
  post '/edit_blog', to: 'create#edit_blog'
  get '/blogs/:id', to: 'blog#show'
  post '/edit_blog_about', to: 'blog#edit_about'
  get '/dashboard', to: 'dashboard#show'
  get '/collaborations', to: 'dashboard#collaborations'
  get '/ideas', to: 'dashboard#ideas'
  get '/stats', to: 'dashboard#stats'
  get '/social', to: 'dashboard#social'
  # get '/collaborations/details', to: 'dashboard#collaboration_item'
  get '/edit_posts', to: 'blog#edit_posts'
  get '/help', to: 'bloghome#help'
  get '/help/export_wordpress', to: 'bloghome#export_wordpress'
  get '/import', to: 'dashboard#import'
  post '/import_xml', to: 'create#import_xml'
  get '/login_page', to: 'home#login_page'
  # get '/themes', to: 'bloghome#themes'

  get '/posts_search', to: 'hack#search'
  get '/tips_search', to: 'tips#search'
  get '/video_search', to: 'video#search'

  get '/get_session_user', to: 'profile#get_session_user'
  post '/check_subdomain', to: 'blog#check_subdomain'

  get '/tips', to: 'tips#show'
  post '/upvote_tip', to: 'tips#upvote'
  post '/follow_tip', to: 'tips#follow'
  post '/create_tip', to: 'tips#create_tip'
  get '/more_tips', to: 'tips#more_tips'

  get '/more_blogposts', to: 'blog#more_blogposts'
  get '/more_categoryposts', to: 'blog#more_categoryposts'

  get '/more_posts', to: 'home#more_hacks'
  get '/more_diys', to: 'hack#more_diys'
  post '/upvote_hack', to: 'hack#upvote'
  post '/follow_hack', to: 'hack#follow'
  post '/delete_post', to: 'hack#delete'
  get '/edit/:id', to: 'edit#edit_post'
  get '/drafts', to: 'dashboard#drafts'
  get '/draft/:id', to: 'edit#draft_post'
  post '/delete_draft', to: 'edit#delete_draft'
  post '/make_edit', to: 'edit#make_edit'
  get '/nift', to: 'nift#show'
  get '/more_nift', to: 'nift#more_nift'
  get '/delete_post/:id', to: 'hack#delete_draft'

  post '/upload', to: 'create#upload'
  post '/single_upload', to: 'create#single_upload'
  post '/tip_upload', to: 'create#tip_upload'
  post '/logo_upload', to: 'create#logo_upload'

  get '/entertainment', to: 'hack#entertainment'
  get '/more_entertainment', to: 'hack#more_entertainment'

  get '/shopping', to: 'hack#shopping'
  get '/more_shopping', to: 'hack#more_shopping'

  get '/inspiration', to: 'hack#inspiration'
  get '/more_inspiration', to: 'hack#more_inspiration'

  get '/video', to: 'video#show'
  get '/more_videos', to: 'video#more_videos'

  get '/qand_a', to: 'qand_a#show'
  get '/more_qanda', to: 'qand_a#more'
  post '/create_question', to: 'qand_a#create_question'
  get '/askquestion', to: 'create#ask'

  get '/fashion', to: 'fashion#show'
  get '/more_fashion', to: 'fashion#more_fashion'

  get '/beauty', to: 'beauty#show'
  get '/more_beauty', to: 'beauty#more_beauty'

  get '/about', to: 'about#about'
  get '/terms', to: 'about#terms'
  get '/privacy', to: 'about#privacy'
  get '/contact', to: 'about#contact'
  get '/jobs', to: 'about#jobs'
  get '/feedback', to: 'about#feedback'
  get '/press', to: 'about#press'
  get '/content', to: 'content#content'

  get '/create', to: 'create#create'
  post '/create_post', to: 'create#create_post'
  post '/save_post', to: 'create#save_post'
  post '/create_submit_post', to: 'create#create_submit_post'
  get '/submitpost', to: 'create#submitpost'
  get '/get_stuff', to: 'home#get_stuff'

  get '/profile', to: 'profile#profile'
  get '/user/:slug', to: 'profile#userslug'
  post '/save_bio', to: 'profile#save_bio'
  post '/sign_in', to: 'profile#sign_in'
  get '/notification', to: 'notification#notification'
  get '/get_more_notifications', to: 'notification#get_more_notifications'
  get '/saved', to: 'profile#saved'
  post '/register_email', to: 'profile#register_email'
  get '/get_popup_timeout', to: 'profile#get_popup_timeout'

  get '/logout', to: 'profile#logout'
  get '*path' => redirect('/')


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
