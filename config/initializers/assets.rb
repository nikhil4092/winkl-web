# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( script.js style.css styles.css bootstrap.min.css bootstrap-tour.css bootstrap-tour.js taggle.css bootstrap.min.js content.js notifications.js quill.js quill.css masonry.js screenshot.js create.js taggle.js instafeed.min.js)
Rails.application.config.assets.precompile << /\.(?:svg|eot|woff|ttf)\z/
