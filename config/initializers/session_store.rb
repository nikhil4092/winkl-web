# Be sure to restart your server when you modify this file.

if Rails.env.production?
    Rails.application.config.session_store :cookie_store, key: '_app_name_session', domain: ".winkl.in"
    Rails.application.config.session_store :cookie_store, key: '_app_name_session', domain: ".winkl.co"
else
    Rails.application.config.session_store :cookie_store, key: '_app_name_session', domain: '.lvh.me'
end
