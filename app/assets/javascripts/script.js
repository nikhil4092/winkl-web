$(document).ready(function () {


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1753312334956150',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
    $('.sidebar-item').click(function(e) {

        items = $('.list-group-item');
        for(i=0; i<items.length; i++){
          item = items[i];
          item.className="list-group-item"
        }
        e.target.className = "list-group-item active"
        e.preventDefault();

    });

    $('.searchbar').click(function(e){
      var modal = document.getElementById('search_popup');
      var span = document.getElementsByClassName("search-close")[0];
      $(window).resize(function() {
        return fit_modal_body($(".search_popup"));
      });
      $('#search_popup').addClass('visible');

      $('.search-text').focus();
      span.onclick = function() {
        $('#search_popup').removeClass('visible');

      }
      window.onclick = function(event) {
          if (event.target == modal) {
            $('#search_popup').removeClass('visible');

          }
      }
      e.preventDefault();
    });

    var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
    $('body').bind(mousewheelevt, function(e){

      if(e.originalEvent.wheelDelta /120 > 0) {

          $('.scroll-navbar').removeClass('nav-up').addClass('nav-down');
          $('.mobile-share').removeClass('move-down').addClass('move-up');
      }
      else{

          $('.scroll-navbar').removeClass('nav-down').addClass('nav-up');
          $('.mobile-share').removeClass('move-up').addClass('move-down');

      }

    });





    $('.back-button').click(function(e){
      history.back();
    });

    var modal = document.getElementById('login_popup');
    var btn = document.getElementById("login");
    var btn2 = document.getElementById("loginslider");
    var span = document.getElementsByClassName("login_close")[0];
    if(btn!=null){
    btn.onclick = function() {
        $('#login_popup').addClass('visible');
        set_size();
      }
    }
    if(btn2!=null){
    btn2.onclick = function() {
      $('#login_popup').addClass('visible');
      set_size();
    }
  }
    span.onclick = function() {
      $('#login_popup').removeClass('visible');

    }
    window.onclick = function(event) {

        if (event.target == modal) {
          $('#login_popup').removeClass('visible');

        }
    }
    var modal = document.getElementById('login_popup');
    var btn = document.getElementById("banner-login");
    var span = document.getElementsByClassName("login_close")[0];
    if(btn!=null){
    btn.onclick = function() {
      $('#login_popup').addClass('visible');
      set_size();
    }
  }
    span.onclick = function() {
      $('#login_popup').removeClass('visible');

    }
    window.onclick = function(event) {

        if (event.target == modal) {
          $('#login_popup').removeClass('visible');

        }
    }

    var modal = document.getElementById('subscribe_popup');
      $('.subscribe-close').click(function(e){
        $('#subscribe_popup').removeClass('visible');
      });
      window.onclick = function(event) {
          if (event.target == modal) {
          $('#subscribe_popup').removeClass('visible');
          }
      }

    $('.hint-card').hover(function(e){
      $(this).animate({
        'right':'0px'
      },200);
    },function(e){
      $(this).animate({
        'right':-$(this).width()+30+'px'
      },200);
    });

});

$('#email-submit').click(function(e){
  // $(this).addClass('email-submitted');
  $this = $(this)
  $(this).text('Loading..');
  $.ajax({
    type: "POST",
    data: {
          email: $('#emailid').val()
          },
    dataType: "json",
    url: "/register_email",
    success: function(data){
      if(data['error']==null){
      $this.text('Done');
      $('#email-pitch').text(data['message']);
    }
    else {
      $this.text('Retry');
      $('#email-pitch').text(data['error']);
    }
    }
  });
});

$('#email-submit2').click(function(e){
  // $(this).addClass('email-submitted');
  $this = $(this)
  $(this).text('Loading..');
  $.ajax({
    type: "POST",
    data: {
          email: $('#emailid2').val()
          },
    dataType: "json",
    url: "/register_email",
    success: function(data){
      if(data['error']==null){
      $this.text('Done');
      $('#email-pitch2').text(data['message']);
    }
    else {
      $this.text('Retry');
      $('#email-pitch2').text(data['error']);
    }
    }
  });
});

$('#email-submit3').click(function(e){
  // $(this).addClass('email-submitted');
  $this = $(this)
  $(this).text('Loading..');
  $.ajax({
    type: "POST",
    data: {
          email: $('#emailid3').val()
          },
    dataType: "json",
    url: "/register_email",
    success: function(data){
      if(data['error']==null){
      $this.text('Done');
      $('#subscribe_popup').removeClass('visible');

    }
    else {
      $this.text('Retry');
      $('#error_field').text(data['message']);
    }
    }
  });
});

$(window).resize(function(){
    var w = window.window.innerWidth;

    if(w<1200){

      $('.horizontal').addClass('hidden');
      $('.vertical').removeClass('hidden');
      $('.hint-card').addClass('hidden');
      $('.profile-stats').addClass('hidden');
      $('.share-layout').css('display','none');
      $('.mobile-share').removeClass('hidden');
      $('.banner-image').addClass('hidden');
      $('.divider-line').addClass('hidden');
      $('.iamblogger').text('I AM A BLOGGER');
        $('.iamblogger').css('width','200px');
        $('.card .list-img').height($('.card .list-img').width());
        $('.card .list-img-blank').height($('.card .list-img').width());
      // $('.subscribe-image').addClass('hidden');
      // $('.user-image').removeClass('col-xs-3').addClass('col-xs-2');
      // $('.user-name').removeClass('col-xs-9').addClass('col-xs-10');
    }
    else {
      $('.horizontal').removeClass('hidden');
      $('.vertical').addClass('hidden');
      $('.hint-card').removeClass('hidden');
      $('.profile-stats').removeClass('hidden');
      $('.share-layout').css('display','block');
      $('.mobile-share').addClass('hidden');
      $('.banner-image').removeClass('hidden');
      $('.divider-line').removeClass('hidden');
      $('.iamblogger').text('LEARN HOW');
      $('.iamblogger').css('width','180px');
      $('.card .list-img').css('height','auto');
      $('.card .list-img-blank').css('height','400px');
      // $('.subscribe-image').removeClass('hidden');

      // $('.user-image').removeClass('col-xs-2').addClass('col-xs-3');
      // $('.user-name').removeClass('col-xs-10').addClass('col-xs-9');
    }

});
var w = window.window.innerWidth;
if(w<1200){
  $('.horizontal').addClass('hidden');
  $('.vertical').removeClass('hidden');
  $('.hint-card').addClass('hidden');
  $('.profile-stats').addClass('hidden');
  $('.share-layout').css('display','none');
  $('.mobile-share').removeClass('hidden');
  $('.banner-image').addClass('hidden');
  $('.divider-line').addClass('hidden');
  $('.iamblogger').text('I AM A BLOGGER');
  $('.iamblogger').css('width','200px');
  $('.card .list-img').height($('.card .list-img').width());
  $('.card .list-img-blank').height($('.card .list-img').width());
  // $('.subscribe-image').addClass('hidden');
}
else {
  $('.horizontal').removeClass('hidden');
  $('.vertical').addClass('hidden');
  $('.hint-card').removeClass('hidden');
  $('.profile-stats').removeClass('hidden');
  $('.share-layout').css('display','block');
  $('.mobile-share').addClass('hidden');
  $('.banner-image').removeClass('hidden');
  $('.divider-line').removeClass('hidden');
  $('.iamblogger').text('LEARN HOW');
  $('.iamblogger').css('width','180px');
  $('.card .list-img').css('height','auto');
  $('.card .list-img-blank').css('height','400px');
  // $('.subscribe-image').removeClass('hidden');

}
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function bookmark_hover(element) {
    element.setAttribute('src', '/images/bookmark-full.svg');
}
function bookmark_unhover(element) {
    element.setAttribute('src', '/images/bookmark-normal.svg');
}

function helpful_hover(element) {
    element.setAttribute('src', '/images/like-full.svg');
}
function helpful_unhover(element) {
    element.setAttribute('src', '/images/like-normal.svg');
}

function chat_hover(element) {
    element.setAttribute('src', '/images/chat-full.svg');
}
function chat_unhover(element) {
    element.setAttribute('src', '/images/chat-onhover.svg');
}

function set_size() {

$('.abcRioButtonLightBlue').height(45);
$('.abcRioButtonLightBlue').css("padding-top","4px");
$('.abcRioButtonLightBlue').width($('#gSignInWrapper').width());
$('.abcRioButtonIcon').css("margin-top","2px");
$('.abcRioButtonIcon').css("margin-left","15px");
$('.abcRioButton').css("box-shadow","none");
$('.abcRioButtonLightBlue').css("border-radius","25px");
$('.abcRioButtonContents').css("margin-left","-10px");
}

function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

var fit_modal_body;

fit_modal_body = function(modal) {
  var body, bodypaddings, header, headerheight, height, modalheight;
  header = $(".modal-header", modal);
  body = $(".modal-body", modal);
  modalheight = parseInt(modal.css("height"));
  headerheight = parseInt(header.css("height")) + parseInt(header.css("padding-top")) + parseInt(header.css("padding-bottom"));
  bodypaddings = parseInt(body.css("padding-top")) + parseInt(body.css("padding-bottom"));
  height = modalheight - headerheight - bodypaddings - 5;
  return body.css("max-height", "" + height + "px");
};


function signOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {

  });
  FB.logout(function(response) {
  // user is now logged out
});
  window.location.href='/logout';
}
$.ajax({
  type: "GET",
  dataType: "json",
  url: "/get_popup_timeout",
  success: function(data){
    if (data['out']==false){
      setTimeout( wait, 20000 );
      function wait() {
          $('#subscribe_popup').addClass('visible');
      }
    }
  }
});
var popup = getParameterByName('subscribe_popup');
if(popup=="true"){
  setTimeout( wait, 2000 );
  function wait() {
      $('#subscribe_popup').addClass('visible');
  }
}
function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function textAreaAdjust(o) {
  o.style.height = "1px";
  o.style.height = (25+o.scrollHeight)+"px";
}

function textAreaAdjustHorizontal(o) {
  $(o).css('width',"50px");
  $(o).css('width',(25+$(o).get(0).scrollWidth)+"px");

}

// $('.tip-card-layout').masonry({
//   // options...
//   itemSelector: '.small-tip',
//   columnWidth: 0
//
// });

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69564268-5', 'auto');
ga('send', 'pageview');
