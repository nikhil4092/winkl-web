class TipsController < ApplicationController
  def show
    @pagenum=5
    @pagename="TIPS"
    if !session['user'].nil?
      @tips = fetch_data(@web_url+"/tips?limit=18&offset=0&user_id="+session['user']['id'].to_s)
    else
      @tips = fetch_data(@web_url+"/tips?limit=18&offset=0")
    end
    render('show')
  end

  def more_tips
    offset = params['offset']
    if !session['user'].nil?
      @newtips = fetch_data(@web_url+"/tips?limit=18&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @newtips = fetch_data(@web_url+"/tips?limit=18&offset="+offset)
    end
    render 'more', layout: false
  end

  # GET /tips_search
  def search
    if !session['user'].nil?
      @newtips = fetch_data(@web_url+"/tips_search?user_id="+session['user']['id'].to_s+"&search="+params['search'])
    else
      @newtips = fetch_data(@web_url+"/tips_search?search="+params['search'])
    end
    render 'more', layout: false
  end

  # POST /create_tip
  def create_tip
    body = {
      'tip[tip]': params['tip'],
      'tip[color]': '#ffffff'
    }
    render json: send_data(@web_url+"/tips",body)
  end

  # POST /upvote
  def upvote
    render json: send_data(@web_url+"/tip_upvote/"+params['id'],{})
  end

  # POST /follow
  def follow
    render json: send_data(@web_url+"/tip_follow/"+params['id'],{})
  end
end
