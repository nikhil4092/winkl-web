class FashionController < ApplicationController
  def show
    @pagenum=2
    @pagename="FASHION"
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/fashion_hacks?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/fashion_hacks?limit=9&offset=0")
    end
    # p (@hacks)
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render('show')
  end

  def more_fashion
    @pagename="FASHION"
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/fashion_hacks?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/fashion_hacks?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render 'hack/more', layout: false
  end

end
