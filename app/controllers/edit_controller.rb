class EditController < ApplicationController
  layout 'edit'
  def edit_post
    @pagename = "Edit post"
    id = params['id']
    if !session['user'].nil?
      @content = fetch_data(@web_url+'/edit_post/'+id)
    end
    if @content.to_s=="Bad credentials".to_s
      redirect_to "/" and return
    else
      @blog = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain)
      if @blog.count >0
        @blog = @blog.first
      else
        redirect_to root_url(subdomain: '')
      end
      if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
        render 'show'
      else
        redirect_to root_url(subdomain: request.subdomain)
      end
    end
  end

  def draft_post
    @pagename = "Draft post"
    id = params['id']
    if !session['user'].nil?
      @content = fetch_data(@web_url+'/post_drafts/'+id)
    end
    if @content.to_s=="Bad credentials".to_s
      redirect_to "/" and return
    else
      @blog = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain)
      if @blog.count >0
        @blog = @blog.first
      else
        redirect_to root_url(subdomain: '')
      end
      if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
        render 'draft'
      else
        redirect_to root_url(subdomain: request.subdomain)
      end
    end
  end

  def delete_draft
    render json: delete_data(@web_url+"/post_drafts/"+params['id'],{})
  end

  def make_edit
    body = {
      'hack[title]': params['title'],
      'hack[hack]': params['hack'],
      'hack[description]': params['description'],
      'hack[posttype]': params['type'],
      'hack[tags]': params['tags'],
      'hack[category]': params['category']
    }
    render json: patch_data(@web_url+"/hacks/"+params['id'],body)
  end
end
