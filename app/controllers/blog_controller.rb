class BlogController < ApplicationController
  before_action :find_blog, only: [:show, :more_blogposts, :category, :more_categoryposts, :getslugcontent, :contact, :about, :about_edit, :edit, :edit_posts, :blog_layout]
  layout :blog_layout

  require 'date'

  def index
    @pagename = "New blog | Winkl"
    @pagedescription = "Create your fashion, beauty or lifestyle blog"
    @blogs = fetch_data(@web_url+"/has_blogs")
    if @blogs.count>0
      redirect_to root_url(subdomain: @blogs.first['subdomain'])
    else
      render 'index'
    end
  end

  def show
      @pagename = @blog['name']
      @pagedescription = @blog['description']
      @hacks = fetch_data(@web_url+"/blog_posts/"+@blog['id'].to_s+"?limit=6&offset=0")
      @active = "Home"
      @hacks.each do |hack|
        date = DateTime.parse(hack['created_at'])
        hack['created_at'] = date.strftime('%A, %d %b')
      end
      if !session['user'].nil? && @blog['blog_user']['id'] == session['user']['id']
        @create = true
      end
      render 'blog/themes/'+@blog['theme'].to_s+'/posts'
  end

  def more_blogposts
      @pagename = @blog['name']
      offset = params['offset']
      @pagedescription = @blog['description']
      @hacks = fetch_data(@web_url+"/blog_posts/"+@blog['id'].to_s+"?limit=6&offset="+offset)
      @active = "Home"
      @hacks.each do |hack|
        date = DateTime.parse(hack['created_at'])
        hack['created_at'] = date.strftime('%A, %d %b')
      end
      if !session['user'].nil? && @blog['blog_user']['id'] == session['user']['id']
        @create = true
      end
      render 'blog/themes/'+@blog['theme'].to_s+'/more', layout: false
  end

  def category
      category = (params['category'].to_s).gsub ' ', '%20'
      @pagename = @blog['name']+" - "+category
      @hacks = fetch_data(@web_url+"/blog_category/"+@blog['id'].to_s+"/"+category.to_s+"?limit=6&offset=0")
      @active = params['category']
      @hacks.each do |hack|
        date = DateTime.parse(hack['created_at'])
        hack['created_at'] = date.strftime('%A, %d %b')
      end
      render 'blog/themes/'+@blog['theme'].to_s+'/categoryposts'
  end

  def more_categoryposts
      offset = params['offset']
      category = (params['category'].to_s).gsub ' ', '%20'
      @pagename = @blog['name']+" - "+category
      @hacks = fetch_data(@web_url+"/blog_category/"+@blog['id'].to_s+"/"+category.to_s+"?limit=6&offset="+offset.to_s)
      @active = params['category']
      @hacks.each do |hack|
        date = DateTime.parse(hack['created_at'])
        hack['created_at'] = date.strftime('%A, %d %b')
      end
      render 'blog/themes/'+@blog['theme'].to_s+'/more', layout: false
  end

def getslugcontent
  @type = "Content"
    slug = params['slug']
    if !session['user'].nil?
      @content = fetch_data(@web_url+'/h/'+slug+'?user_id='+session['user']['id'].to_s)
      @hacks = fetch_data(@web_url+"/random_hacks?limit=6&user_id="+session['user']['id'].to_s)
    else
      @content = fetch_data(@web_url+'/h/'+slug)
      @hacks = fetch_data(@web_url+"/random_hacks?limit=6")
    end
    @hacks.each do |hack|
      date = DateTime.parse(hack['created_at'])
      hack['created_at'] = date.strftime('%A, %d %b')
    end
    date = DateTime.parse(@content['created_at'])
    @content['created_at'] = date.strftime('%A, %d %b')

    @pagename=@content['title']
    @pagedescription = @content['description']
    if @content['image'].nil? || @content['image'].empty?
      src = /<img.*?src=['"](.*?)['"]/.match(@content['hack'])
      if src.nil?
        @content['image'] = ''
      else
        @content['image'] = src[1]
      end
    end
    render('content/content')
end

def check_subdomain
  render json: fetch_data(@web_url+"/check_subdomain/"+params['subdomain'].to_s)
end

def follow
  render json: send_data(@web_url+"/blog_follow/"+params['id'].to_s,{})
end

def contact
    @pagename = @blog['name']+" - Contact"
    render('contact')
end

def about
    @pagename = @blog['name']+" - About"
    if @blog['bio'].nil?
      @blog['bio']=""
    end
    @active = "About"
    render('about')
end

def about_edit

  @pagename = @blog['name']+" - Edit your information"
  if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
    if @blog['bio'].nil?
      @blog['bio']=""
    end
    render('about_edit')
  else
    redirect_to root_url(subdomain: '')
  end
end

def edit_about
  body = {
    'blogid': params['blogid'],
    'bio': params['bio'],
    'facebook': params['facebook'],
    'instagram': params['instagram'],
    'twitter': params['twitter'],
    'pinterest': params['pinterest'],
    'phone': params['phone'],
    'email': params['email'],
  }
  render json: send_data(@web_url+"/edit_blog_about",body)

end

def edit
  @pagename = @blog['name']+" - Edit your Winkl blog"
  if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
    render('edit')
  else
    redirect_to root_url(subdomain: request.subdomain)
  end
end

def edit_posts
  @pagename = @blog['name']+" - Edit your posts"
  if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
    @userhacks = fetch_data(@web_url+"/user_posts/"+session['user']['id'].to_s)
    @userhacks.each do |hack|
      date = DateTime.parse(hack['created_at'])
      hack['created_at'] = date.strftime('%A, %d %b')
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
    end
    render('edit_posts')
  else
    redirect_to root_url(subdomain: request.subdomain)
  end
end

def blog_layout
  "themes/"+@blog['theme'].to_s
end

private
  def find_blog
    if request.subdomain == 'www'
      req = request.host[4..-1]
    else
      req = request.host
    end
    if !session['user'].nil?
      blogs = fetch_data(@web_url+"/customblogs?customdomain="+req+"&user_id="+session['user']['id'].to_s)
    else
      blogs = fetch_data(@web_url+"/customblogs?customdomain="+req)
    end
    if blogs.length > 0
      @blog = blogs.first
    else
      if !session['user'].nil?
        blogs = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain+"?user_id="+session['user']['id'].to_s)
      else
        blogs = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain)
      end
      if blogs.length > 0
        @blog = blogs.first
      end
    end
    redirect_to root_url(subdomain: 'www') unless @blog
    if @blog['insta_userid']!="" && @blog['insta_accesstoken']!=""
      crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
      @blog['insta_userid'] = crypt.decrypt_and_verify(@blog['insta_userid'])
      @blog['insta_accesstoken'] = crypt.decrypt_and_verify(@blog['insta_accesstoken'])
    end
  end

end
