class HackController < ApplicationController
  def show
    @pagenum=7
    @pagename="DIYs & TUTORIALS"
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/diy_hacks?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/diy_hacks?limit=9&offset=0")
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render('show')
  end

  def more_diys
    @pagename="DIYs & TUTORIALS"
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/diy_hacks?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/diy_hacks?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render 'more', layout: false
  end

  def entertainment
    @pagenum=7
    @pagename="ENTERTAINMENT"
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/entertainment?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/entertainment?limit=9&offset=0")
    end
    # p (@hacks)
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render('entertainment')
  end

  def more_entertainment
    @pagename="ENTERTAINMENT"
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/entertainment?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/entertainment?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render 'more', layout: false
  end

  def shopping
    @pagenum=7
    @pagename="SHOPPING"
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/shopping?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/shopping?limit=9&offset=0")
    end
    # p (@hacks)
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render('shopping')
  end

  def more_shopping
    @pagename="SHOPPING"
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/shopping?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/shopping?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render 'more', layout: false
  end

  def inspiration
    @pagenum=7
    @pagename="INSPIRATION"
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/inspiration?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/inspiration?limit=9&offset=0")
    end
    # p (@hacks)
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render('inspiration')
  end

  def more_inspiration
    @pagename="INSPIRATION"
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/inspiration?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/inspiration?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']=@pagename
    end
    render 'more', layout: false
  end

  # GET /posts_search
  def search
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/hacks_search?user_id="+session['user']['id'].to_s+"&search="+params['search'])
    else
      @hacks = fetch_data(@web_url+"/hacks_search?search="+params['search'])
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']= hack['posttype'].split(',')[0]
    end
    render 'more', layout: false
  end

  def delete_draft
    @pagename = "Drafts"
    @drafts = delete_data(@web_url+"/post_drafts/"+params['id'].to_s,{})
    redirect_to "/drafts" and return
  end

  # POST /upvote
  def upvote
    render json: send_data(@web_url+"/hack_upvote/"+params['id'],{})
  end

  # POST /follow
  def follow
    render json: send_data(@web_url+"/hack_follow/"+params['id'],{})
  end

  # POST /delete
  def delete
    render json: delete_data(@web_url+"/hacks/"+params['id'],{})
  end
end
