class TutorialController < ApplicationController
  def show
    @pagenum=8
    @pagename="TUTORIALS"
    if !session['user'].nil?
      @videos = fetch_data(@web_url+"/tutorials?user_id="+session['user']['id'].to_s)
    else
      @videos = fetch_data(@web_url+"/tutorials")
    end
    render('show')
  end
end
