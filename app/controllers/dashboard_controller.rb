class DashboardController < ApplicationController
  before_action :find_blog, only: [:show, :collaborations, :collaboration_item, :ideas, :stats, :social, :drafts, :import]
  layout 'dashboard'
  def show
    @pagename = "Manage your blog | Winkl"
    @pagedescription = "Edit your drafts | Customize your blog | Import your Wordpress blog | Check your stats | Work on collaborations"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']

      @create = true
      render('dashboard')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def collaborations
    @pagename = "Collaborate with brands"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
      @collabs = [
        {
          image: "https://s-media-cache-ak0.pinimg.com/736x/d8/3a/c3/d83ac31d791d9c9aef46f89729f0cf90.jpg",
          date: "June 25, 17",
          title: "Launch of the new Levis Jeans collection.",
          description: "Shoot with the new Levis Jeans collection and talk about it on your blog and social channels.",
          type: "PAID",
        },
        {
          image: "http://pkvogue.com/wp-content/uploads/2016/10/Tanieya-Khanuja-Spring-Collection-at-amazon-india-fashion-week-2017-17.jpg",
          date: "June 12, 17",
          title: "Invite to the unveiling of the spring collection",
          description: "Shoot with the new collection and post it on social media",
          type: "BARTER",
        },
        {
          image: "http://www.glamour.com/images/beauty/2013/07/beauty-products-mg-main.jpg",
          date: "July 01, 17",
          title: "Review the latest beauty products",
          description: "Shoot with the new collection and post it on social media",
          type: "BARTER",
        }
      ]
      @create = true
      render('collaboration')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def collaboration_item
    @pagename = "Collaborate with brands"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']

      @create = true
      render('collaboration_item')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def ideas
    @pagename = "Inspiration for your next blog post"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']

      @create = true
      @ideas = [
        {
          title: "Your Fashion Wishlist",
          description: "",
          up: ""
        },
        {
          title: "Take a few photos of your latest wardrobe additions and give style advice",
          description: "",
          up: ""
        },
        {
          title: "Come up with outfits for a special occasion (Vacation, Party, Girls Weekend,…)",
          description: "",
          up: "300"
        },
        {
          title: "Connect with a likeminded Blogger and find a piece you both style differently",
          description: "",
          up: "1200"
        },
        {
          title: "Pick one Keypiece from your wardrobe and find ways to wear it throughout the week in unexpected ways",
          description: "",
          up: ""
        },
        {
          title: "You favorite Trends of the Season, or the least favorite ones",
          description: "",
          up: ""
        },
        {
          title: "Imitate a Celebrity Fashion Look in a budget-friendly way",
          description: "",
          up: "800"
        },
      ]
      render('ideas')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def stats
    @pagename = "Stats & Analytics for your blog"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']

      @create = true
      render('stats')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def social
    @pagename = "Connect your social accounts"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
      @create = true
      render('social')
    else
      redirect_to root_url(subdomain: '')
    end
  end

  def drafts
    @pagename = "Edit & save your drafts"
    @drafts = fetch_data(@web_url+"/post_drafts")
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
      @create = true
      render 'drafts'
    else
      redirect_to root_url(subdomain: '')
    end

  end

  def import
    @pagename ="Export content from your Wordpress blog to Winkl"
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
      render 'import'
    else
      redirect_to root_url(subdomain: '')
    end
  end

  private
  def find_blog
    if request.subdomain == 'www'
      req = request.host[4..-1]
    else
      req = request.host
    end
    if !session['user'].nil?
      blogs = fetch_data(@web_url+"/customblogs?customdomain="+req+"&user_id="+session['user']['id'].to_s)
    else
      blogs = fetch_data(@web_url+"/customblogs?customdomain="+req)
    end
    if blogs.length > 0
      @blog = blogs.first
    else
      if !session['user'].nil?
        blogs = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain+"?user_id="+session['user']['id'].to_s)
      else
        blogs = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain)
      end
      if blogs.length > 0
        @blog = blogs.first
      end
    end
    redirect_to root_url(subdomain: 'www') unless @blog
    if @blog['insta_userid']!="" && @blog['insta_accesstoken']!=""
      crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
      @blog['insta_userid'] = crypt.decrypt_and_verify(@blog['insta_userid'])
      @blog['insta_accesstoken'] = crypt.decrypt_and_verify(@blog['insta_accesstoken'])
    end
  end

end
