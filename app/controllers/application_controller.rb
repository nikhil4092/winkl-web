class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_action :set_url, :check_blog

  def fetch_data (url)
    if session['user'].nil?
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '}
    else
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '+session['user']['auth_token'] }
    end
    data = HTTParty.get(url, headers: headers)
    return data
  end

  def send_data (url, body)
    if session['user'].nil?
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '}
    else
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '+session['user']['auth_token'] }
    end
    data = HTTParty.post(url,body: body,headers: headers)
    return data
  end

  def delete_data (url, body)
    if session['user'].nil?
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '}
    else
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '+session['user']['auth_token'] }
    end
    data = HTTParty.delete(url,body: body,headers: headers)
    return data
  end

  def patch_data (url, body)
    if session['user'].nil?
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '}
    else
      headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt '+session['user']['auth_token'] }
    end
    data = HTTParty.patch(url,body: body,headers: headers)
    return data
  end

  def set_url
    @web_url = "https://api.winkl.in"
  end

  def check_blog
    @blogs = fetch_data(@web_url+"/has_blogs")
    if @blogs.length > 0
      @blog = @blogs.first
    end
  end
end
