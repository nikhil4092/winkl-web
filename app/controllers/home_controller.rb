class HomeController < ApplicationController
  require 'date'
  def show
    @pagenum=1
    @pagename="Fashion, beauty & lifestyle from the best bloggers & influencers"
    @pagedescription = "Read, share and discover the best stories, experiences, reviews and looks from fashion, beauty & lifestyle bloggers from around the world."
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/hacks?limit=9&offset=0&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/hacks?limit=9&offset=0")
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']= hack['posttype'].split(',')[0]
      date = DateTime.parse(hack['created_at'])
      hack['created_at'] = date.strftime('%A, %d %b')
    end
    render('home')
  end

  def more_hacks
    offset = params['offset']
    if !session['user'].nil?
      @hacks = fetch_data(@web_url+"/hacks?limit=9&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @hacks = fetch_data(@web_url+"/hacks?limit=9&offset="+offset)
    end
    @hacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
      hack['posttype']= hack['posttype'].split(',')[0]
      date = DateTime.parse(hack['created_at'])
      hack['created_at'] = date.strftime('%A, %d %b')
    end
    render 'hack/more', layout: false
  end

  def login_page
    @pagename = "Login"
    @from = params['from']
    render 'login_page'
  end

end
