class NotificationController < ApplicationController

  # GET /notification
  def notification
    @pagename="Notifications"
    @notifications = fetch_data(@web_url+"/notifications?limit=30&offset=0")
    render('notification')
  end

  # GET /get_more_notifications
  def get_more_notifications
    @pagename="Notifications"
    offset = params['offset']
    @notifications = fetch_data(@web_url+"/notifications?limit=30&offset="+offset)
    render 'more', layout: false
  end
end
