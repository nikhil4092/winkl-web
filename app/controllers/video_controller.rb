class VideoController < ApplicationController
  def show
    @pagenum=6
    @pagename="VIDEOS"
    if !session['user'].nil?
      @videos = fetch_data(@web_url+"/video_posts?limit=10&offset=0&user_id="+session['user']['id'].to_s)
    else
      @videos = fetch_data(@web_url+"/video_posts?limit=10&offset=0")
    end
    @videos.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
    end
    render('show')
  end

  # GET /more_videos
  def more_videos
    offset = params['offset']
    if !session['user'].nil?
      @videos = fetch_data(@web_url+"/video_posts?limit=10&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      @videos = fetch_data(@web_url+"/video_posts?limit=10&offset="+offset)
    end
    @videos.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
    end
    render 'more', layout: false
  end

  # GET /video_search
  def search
    if !session['user'].nil?
      @videos = fetch_data(@web_url+"/video_search?user_id="+session['user']['id'].to_s+"&search="+params['search'])
    else
      @videos = fetch_data(@web_url+"/video_search?search="+params['search'])
    end
    render 'more', layout: false
  end
end
