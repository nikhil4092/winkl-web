# require "aws-sdk"

class CreateController < ApplicationController

  layout 'create'
  def create
    @pagename="CREATE"
    @blog = fetch_data(@web_url+"/subdomainblogs/"+request.subdomain)
    if @blog.count >0
      @blog = @blog.first
    else
      redirect_to root_url(subdomain: '')
    end
    if !@blog.nil? && !session['user'].nil? && !@blog['blog_user'].nil? && @blog['blog_user']['id']==session['user']['id']
      render('create')
    else
      redirect_to root_url(subdomain: request.subdomain)
    end

  end

  def submitpost
    @pagename="Submit Post"
    render('submitpost')
  end

  def ask
    @pagename="Ask Question"
    render('qand_a/ask')
  end

  def save_post
    if params['id'].nil?
      body = {
        'post_draft[title]': params['title'],
        'post_draft[hack]': params['hack'],
        'post_draft[description]': params['description'],
        'post_draft[extrascript]': params['extrascript'],
      }
      render json: send_data(@web_url+"/post_drafts",body)

    else
      body = {
        'post_draft[title]': params['title'],
        'post_draft[hack]': params['hack'],
        'post_draft[description]': params['description'],
        'post_draft[extrascript]': params['extrascript']
      }
      render json: patch_data(@web_url+"/post_drafts/"+params['id'],body)
    end
  end

  def create_post
    body = {
      'hack[title]': params['title'],
      'hack[hack]': params['hack'],
      'hack[description]': params['description'],
      'hack[posttype]': params['type'],
      'hack[tags]': params['tags'],
      'hack[extrascript]': params['extrascript'],
      'hack[category]': params['category']
    }
    render json: send_data(@web_url+"/hacks",body)
  end

  def create_submit_post
    body = {
      'hack[title]': params['title'],
      'hack[hack]': params['hack'],
      'hack[url]': params['url'],
      'hack[image]': params['image'],
      'hack[posttype]': params['type'],
      'hack[tags]': params['tags']
    }
    render json: send_data(@web_url+"/hacks",body)
  end

  def blog
    body = {
      'blog[name]': params['name'],
      'blog[description]': params['description'],
      'blog[subdomain]': params['subdomain'],
      'blog[theme_color]': params['theme_color'],
      'blog[layout]': params['layout'],
      'blog[logo]': params['logo'],
      'blog[facebook]': params['facebook'],
      'blog[instagram]': params['instagram'],
      'blog[twitter]': params['twitter'],
      'blog[pinterest]': params['pinterest'],
      'blog[category]': params['category'],
      'blog[text_color]': params['text_color'],
      'blog[showheading]': params['showheading'],
      'blog[showdescription]': params['showdescription'],
      'blog[showlogo]': params['showlogo'],
      'blog[logosize]': params['logosize']
    }
    render json: send_data(@web_url+"/blogs",body)
  end

  def edit_blog
    body = {
      'blog[name]': params['name'],
      'blog[description]': params['description'],
      'blog[subdomain]': params['subdomain'],
      'blog[theme_color]': params['theme_color'],
      'blog[layout]': params['layout'],
      'blog[logo]': params['logo'],
      'blog[facebook]': params['facebook'],
      'blog[instagram]': params['instagram'],
      'blog[twitter]': params['twitter'],
      'blog[pinterest]': params['pinterest'],
      'blog[category]': params['category'],
      'blog[text_color]': params['text_color'],
      'blog[showheading]': params['showheading'],
      'blog[showdescription]': params['showdescription'],
      'blog[showlogo]': params['showlogo'],
      'blog[logosize]': params['logosize']
    }
    render json: patch_data(@web_url+"/blogs/"+params['id'].to_s,body)
  end

  def upload
    data = params['data'].to_s
    title = params['title'].to_s
    image_params = data.split(',')
    size = image_params.length-1
    i = 0
    output = {}
    count = 0
    while i <= size do
      temp_type = image_params[i].split('"')[2]
      temp_data = image_params[i+1]
      temp_data = temp_data.split("\\\"\"")[0]
      imageExtension = temp_type.split(';')[0].split('/')
      extension = imageExtension[imageExtension.length - 1]
      url = send_img(Time.now,extension,temp_data,title)
      output[''+count.to_s] = url
      count += 1
      i +=2
    end
    render json: output
  end

  def single_upload
    data = params['data'].to_s
    title = params['title'].to_s
    image_params = data.split(',')
    size = image_params.length-1
    i = 0
    output = {}
    while i <= size do
      temp_type = image_params[i].split('"')[1]
      temp_data = image_params[i+1]
      temp_data = temp_data.split("\\\"\"")[0]
      imageExtension = temp_type.split(';')[0].split('/')
      extension = imageExtension[imageExtension.length - 1]
      url = send_img(Time.now,extension,temp_data,title)
      output['data'] = data
      output['url'] = url
      i +=2
    end
    render json: output
  end

  def tip_upload
    data = params['data'].to_s
    title = params['title'].to_s
    image_params = data.split(',')
    size = image_params.length-1
    i = 0
    output = {}
    while i <= size do
      temp_type = image_params[i].split('"')[0]
      temp_data = image_params[i+1]
      temp_data = temp_data.split("\\\"\"")[0]
      imageExtension = temp_type.split(';')[0].split('/')
      extension = imageExtension[imageExtension.length - 1]
      url = send_img(Time.now,extension,temp_data,title)
      output['data'] = data
      output['url'] = url
      i +=2
    end
    render json: output
  end

  def logo_upload
    data = params['data'].to_s
    title = params['title'].to_s
    image_params = data.split(',')
    size = image_params.length-1
    i = 0
    output = {}
    while i <= size do
      temp_type = image_params[i].split('"')[0]
      temp_data = image_params[i+1]
      temp_data = temp_data.split("\\\"\"")[0]
      imageExtension = temp_type.split(';')[0].split('/')
      extension = imageExtension[imageExtension.length - 1]
      url = send_logoimg(Time.now,extension,temp_data,title)
      output['data'] = data
      output['url'] = url
      i +=2
    end
    render json: output
  end

  def send_logoimg(name,extension,data,title)
    require 'aws-sdk-v1'
    s3 = AWS::S3.new(:access_key_id => "AKIAJ2DZOYQQELKBONZA",:secret_access_key => "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")
    bucket = s3.buckets['winkl-production']
    data = data.gsub(/^data:image\/\w+;base64,/, '')
    data = Base64.decode64(data.to_s)
    type = 'image/'+extension.to_s
    name = name.to_s + ".#{extension}"
    obj = bucket.objects.create('logo_images/'+title+'/'+name,data,{content_type:type,acl:"public_read"})
    url = obj.public_url().to_s
    return url
  end

  def import_xml
    data = params['data'].to_s
    data = data.gsub(/^data:text\/\w+;base64,/, '')
    data = Base64.decode64(data)
    p "||"
    p data
    body = {
      data: data
    }
    render json: send_data(@web_url+"/import_posts",body)
  end

  def send_img(name,extension,data,title)
    require 'aws-sdk-v1'
    s3 = AWS::S3.new(:access_key_id => "AKIAJ2DZOYQQELKBONZA",:secret_access_key => "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")
    bucket = s3.buckets['winkl-production']
    data = data.gsub(/^data:image\/\w+;base64,/, '')
    data = Base64.decode64(data.to_s)
    type = 'image/'+extension.to_s
    name = name.to_s + ".#{extension}"
    obj = bucket.objects.create('content_images/'+title+'/'+name,data,{content_type:type,acl:"public_read"})
    url = obj.public_url().to_s
    return url
  end
end
