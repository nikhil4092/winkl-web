class QandAController < ApplicationController
  def show
    @pagenum=4
    @pagename="Q&A"
    headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt' }
    if !session['user'].nil?
      @qandas = fetch_data(@web_url+"/questions?limit=10&offset=0&user_id="+session['user']['id'].to_s)
    else
      @qandas = fetch_data(@web_url+"/questions?limit=10&offset=0")
    end
    @offset = @qandas.length
    render('show')
  end

  def create_question
    body = {
      'question[user_id]': 1,
      'question[question]': params['question'],
      'question[description]': params['description'],
      'tags': '',
      'products': ''
    }
    render json: send_data(@web_url+"/questions",body)
  end

  # GET /more
  def more
    headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt' }
    offset = params['offset']
    if !session['user'].nil?
      qandas = fetch_data(@web_url+"/questions?limit=10&offset="+offset+"&user_id="+session['user']['id'].to_s)
    else
      qandas = fetch_data(@web_url+"/questions?limit=10&offset="+offset)
    end
    render json: @qandas
  end
end
