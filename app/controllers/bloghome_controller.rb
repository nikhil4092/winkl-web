class BloghomeController < ApplicationController

  layout 'bloghome'

  def landing
    @pagename = "Create a fashion blog for free in 3 minutes | All themes for free"
    @pagedescription = "Easy blog setup & No plugins | Beautiful themes for free | Grow your audience | Collaborate with brands & photographers | Become an influencer"
    render 'blog/landing'
  end

  def help
    @pagename = "Need help with your Winkl blog? Call us."
    render 'blog/help'
  end

  def themes
    @pagename = "Themes"
    render 'blog/themes'
  end

  def export_wordpress
    @pagename = "Export your wordpress blog to winkl"
    render 'blog/export_wordpress'
  end

  def add_insta
    @pagename = "Add Insta"
    render 'blog/addinsta'
  end

end
