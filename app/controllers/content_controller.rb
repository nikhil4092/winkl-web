class ContentController < ApplicationController
  before_action :set
  require 'date'

  def content
    @pagename="CONTENT"
    render('content')
  end

  def getslugcontent
    @type = "Content"
    slug = params['slug']
    if !session['user'].nil?
      @content = fetch_data(@web_url+'/h/'+slug+'?user_id='+session['user']['id'].to_s)
      @hacks = fetch_data(@web_url+"/random_hacks?limit=6&user_id="+session['user']['id'].to_s)
    else
      @content = fetch_data(@web_url+'/h/'+slug)
      @hacks = fetch_data(@web_url+"/random_hacks?limit=6")
    end
    date = DateTime.parse(@content['created_at'])
    @content['created_at'] = date.strftime('%A, %d %b')
    @pagename=@content['title']
    @pagedescription = @content['description']
    if @content['image'].nil? || @content['image'].empty?
      src = /<img.*?src=['"](.*?)['"]/.match(@content['hack'])
      if src.nil?
        @content['image'] = ''
      else
        @content['image'] = src[1]
      end
    end
    render('content')
  end

  def set
    @type = 'content'
  end
end
