class ProfileController < ApplicationController
  require 'bcrypt'

  def profile
    @pagename="Profile"
    if !session['user'].nil?
      @userhacks = fetch_data(@web_url+"/user_posts/"+session['user']['id'].to_s)
      @userhacks.each do |hack|
        if hack['image'].nil? || hack['image'].empty?
          src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
          if src.nil?
            hack['image'] = ''
          else
            hack['image'] = src[1]
          end
        end
      end
      # @userqandas = fetch_data(@web_url+"/questions?limit=10&offset=0&user_id="+session['user']['id'].to_s)
      @usertips = fetch_data(@web_url+"/user_tips/"+session['user']['id'].to_s)
      @usernotifications = fetch_data(@web_url+"/notifications?limit=10&offset=0")
      # @userbadges = fetch_data(@web_url+"/badge?user_id="+session['user']['id'].to_s)
      @user = fetch_data(@web_url+"/users/"+session['user']['id'].to_s)
    end
    render('profile')
  end

  def userslug
    @user = [] << fetch_data(@web_url+"/u/"+params[:slug])
    @pagename= @user[0]['full_name'].to_s+"'s profile"
      @userhacks = fetch_data(@web_url+"/other_user_posts/"+(@user[0]['id'].to_s))
      @userhacks.each do |hack|
        if hack['image'].nil? || hack['image'].empty?
          src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
          if src.nil?
            hack['image'] = ''
          else
            hack['image'] = src[1]
          end
        end
      # @userqandas = fetch_data(@web_url+"/questions?limit=10&offset=0&user_id="+session['user']['id'].to_s)
      @usertips = fetch_data(@web_url+"/user_tips/"+@user[0]['id'].to_s)
      @usernotifications = fetch_data(@web_url+"/notifications?limit=10&offset=0")
      # @userbadges = fetch_data(@web_url+"/badge?user_id="+session['user']['id'].to_s)
    end
    render('otherprofile')
  end

  # GET /saved
  def saved
    @pagename = 'Saved posts'
    @savedhacks = fetch_data(@web_url+"/saved_hacks/"+session['user']['id'].to_s)
    @savedhacks.each do |hack|
      if hack['image'].nil? || hack['image'].empty?
        src = /<img.*?src=['"](.*?)['"]/.match(hack['hack'])
        if src.nil?
          hack['image'] = ''
        else
          hack['image'] = src[1]
        end
      end
    end
    @savedtips = fetch_data(@web_url+"/saved_tips/"+session['user']['id'].to_s)
    @savedvideos = fetch_data(@web_url+"/saved_videos/"+session['user']['id'].to_s)
    render 'saved'
  end

  # POST /save_bio
  def save_bio
    body = {
      'bio': params['bio'],
      'id': session['user']['id'],
    }
    @profile = send_data(@web_url+"/savebio",body)
    render json: @profile
  end

  #POST /register_email
  def register_email
    body = {
      'email': params['email']
    }
    email = params['email'].match(/\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i)
    if !email.nil?
      @email = send_data(@web_url+"/register_email",body)
      render json: @email
      session['subscribe']=true
    else
      render json: {
        error: "Invalid email id"
      }
    end
  end

  def addinsta
    @pagename = "Add Insta"
    code = params['code']
    if code !=""
      body = {
        client_id: 'c30104825d8d47f18b0a1dea317bfd86',
        client_secret: 'fcebfe6c8448402eb4383363dc1f7f71',
        grant_type: 'authorization_code',
        redirect_uri: 'http://www.winkl.co/addinstatowinkl',
        code: code
      }
      data = HTTParty.post('https://api.instagram.com/oauth/access_token',body: body)
      crypt = ActiveSupport::MessageEncryptor.new(Rails.application.secrets.secret_key_base)
        token = crypt.encrypt_and_sign(data['access_token'])
        id = crypt.encrypt_and_sign(data['user']['id'])
        save_body = {
          token: token,
          id: id
        }
        output = send_data(@web_url+"/addinsta",save_body)
        redirect_to('http://'+output['subdomain']+'.winkl.co')
    else
      render 'blog/addinsta'
    end
  end

  # POST sign_in
  def sign_in
  headers = { "Authorization" => 'Token ruor7REQi9KJz6wIQKDXvwtt' }
  body = {
    'user[full_name]': params['full_name'],
    'user[email]': params['email'],
    'user[gender]': params['gender'],
    'user[profile_pic]': params['profile_pic'],
    'user[device_id]': '',
    'user[fb_id]': params['fb_id']
  }
  @profile = HTTParty.post(@web_url+"/users",body: body,
  headers: headers)
  if !@profile.empty?
    session['login']=true
    session['user']=@profile
  end
  render json: @profile

  end

  # GET /logout
  def logout
    session.clear
    reset_session
    redirect_to :back
  end

  def get_session_user
    render json: session['user']
  end

  def get_popup_timeout
    output = {}
    if !cookies['popup'].nil? || !session['user'].nil? || !session['subscribe'].nil?
      output['out']=true
    else
      output['out']=false
    end
    render json: output
    if(cookies['popup'].nil?)
      cookies['popup'] = {
        value: Time.now,
        expires: 1.hour.from_now
      }
    end
  end

end
