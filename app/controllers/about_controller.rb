class AboutController < ApplicationController
  def about
    @pagenum=2
    @pagename="About Us | Winkl"
    @pagedescription = "Winkl believes in the power of community. It helps bloggers become influencers and successful."
    render('about')
  end

  def jobs
    @pagename="JOBS | Winkl"
    render('jobs')
  end

  def privacy
    @pagename="Privacy Policy | Winkl"
    render('privacy')
  end

  def terms
    @pagename="TERMS | Winkl"
    render('terms')
  end

  def contact
    @pagename="CONTACT | Winkl"
    render('contact')
  end

  def press
    @pagename="PRESS | Winkl"
    render('press')
  end

  def feedback
    @pagename="FEEDBACK | Winkl"
    render('feedback')
  end
end
