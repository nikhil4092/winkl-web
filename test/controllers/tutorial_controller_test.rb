require 'test_helper'

class TutorialControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get tutorial_show_url
    assert_response :success
  end

end
